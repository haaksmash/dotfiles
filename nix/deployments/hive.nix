let
  nasIp = "192.168.1.168";
  serverIp = "192.168.1.44";
in
{
  meta = {
    name = "house";
    description = "machines used in my house";
  };

  defaults = { pkgs, ... }:
    {
      imports = [
        # make sure you have properly added the home-manager channel!
        <home-manager/nixos>
      ];

      documentation.enable = false;

      # for nixops to log in and perform operations as haak (instead of root)
      security.sudo.wheelNeedsPassword = false;
      home-manager.users.haak = (import ../home/commandline.nix);

      # Configure the root account so that i can get in there from the framework laptop
      users.users.root.openssh.authorizedKeys.keys = [
        (import ../system/framework/keys.nix).ssh.public
      ];
      users.extraUsers.root = {
        shell = pkgs.zsh;
      };

      environment.systemPackages = with pkgs; [
        tmux
        ripgrep
        htop
      ];

      # Optimize nix store by hardlinking identitical files.
      nix.settings.auto-optimise-store = true;

      # Limit the systemd journal to 100 MB of disk or the
      # last 7 days of logs, whichever happens first.
      services.journald.extraConfig = ''
        SystemMaxUse=100M
        MaxFileSec=7day
      '';

      # servers should not auto-upgrade; it leads to strange behavior when they
      # reboot, because of creating their own, new, generation outside of
      # nixops management.
      system.autoUpgrade.enable = false;
    };

  server =
    { nodes, ... }:
    {
      deployment.targetHost = serverIp;
      imports = [
        ../system/xps11/configuration.nix
        ../home/programs/nginx
        ../home/programs/jellyfin
        ../home/programs/calibre-web
        ../home/programs/forgejo/webserver.nix
      ];

      fileSystems."/storage" = {
        device = "${nasIp}:/storage";
        fsType = "nfs";
        options = [ "x-systemd.automount" "noauto" "_netdev" ];
      };
    };

  nas =
    { pkgs, ... }:
    {
      deployment.targetHost = nasIp;
      imports = [
        ../system/svalbard/configuration.nix
        ../home/programs/qbittorrent
        ../home/programs/beets
        ../home/programs/prowlarr
        ../home/programs/bazarr
        ../home/programs/radarr
        ../home/programs/sonarr
        ../home/programs/readarr
        ../home/programs/calibre
        ../home/programs/forgejo/backendserver.nix
      ];

      fileSystems."/export/storage" =
        {
          device = "/storage";
          options = [ "bind" ];
        };
      services.nfs.server.enable = true;
      services.nfs.server.exports = ''
        /export 192.168.1.0/24(insecure,rw,sync,crossmnt,no_subtree_check,fsid=0)
        /export/storage 192.168.1.0/24(rw,sync,nohide,no_subtree_check,insecure)
      '';

      systemd.tmpfiles.rules = [
        "d /var/lib/nfs/nfsdcld 755 root root"
      ];

      networking.firewall = {
        allowedTCPPorts = [ 2049 ]; # NFS port
        allowedUDPPorts = [ 51820 ]; # Clients and peers can use the same port, see listenport
      };

      # Enable WireGuard
      networking.wg-quick.interfaces =
        let
          # Public key of the server (not a file path).
          publicKey = "5y66WShsFXqM5K7/4CPEGCWfk7PQyNhVBT2ILjbGm2I=";
        in
        {
          # "wg0" is the network interface name. You can name the interface arbitrarily.
          wg0 = {
            # Determines the IP address and subnet of the client's end of the tunnel interface.
            address = [ "10.64.168.213/32" "fc00:bbbb:bbbb:bb01::1:a8d4/128" ];
            dns = [ "10.64.0.1" ];
            listenPort = 51820; # to match firewall allowedUDPPorts (without this wg uses random port numbers)

            # Note: The private key can also be included inline via the privateKey option,
            # but this makes the private key world-readable; thus, using privateKeyFile is
            # recommended.
            #privateKeyFile = "/run/keys/wireguard";
            # See https://nixos.wiki/wiki/WireGuard#Tunnel_does_not_automatically_connect_despite_persistentKeepalive_being_set
            # postUp = [ "wg set wg0 peer ${publicKey} persistent-keepalive 25" ];
            privateKey = builtins.readFile ../../secrets/wireguard/svalbard;

            # For a client configuration, one peer entry for the server will suffice.
            peers = [
              # Sweden, Malmo, se15
              {
                inherit publicKey;

                # Forward all the traffic via VPN.
                allowedIPs = [ "0.0.0.0/0" "::0/0" ];

                # Set this to the server IP and port.
                endpoint = "193.138.218.80:51820"; # ToDo: route to endpoint not automatically configured https://wiki.archlinux.org/index.php/WireGuard#Loop_routing https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577

                # Send keepalives every 25 seconds. Important to keep NAT tables alive.
                # see https://nixos.wiki/wiki/WireGuard#Tunnel_does_not_automatically_connect_despite_persistentKeepalive_being_set
                persistentKeepalive = 25;
              }
            ];
          };
        };
    };
}
