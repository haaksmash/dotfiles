#! /bin/sh

install_home_manager() {
  nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
  nix-channel --update

  export NIX_PATH=$HOME/.nix-defexpr/channels${NIX_PATH:+:}$NIX_PATH
  nix-shell '<home-manager>' -A install
  mkdir -p ~/.config/home-manager

  parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
  cd "$parent_path"
  [ -f ~/.config/home-manager/home.nix ] && mv ~/.config/home-manager/home.nix ~/.config/home-manager/home.nix.orig
  ln -s ./home/commandline.nix ~/.config/home-manager/home.nix
  home-manager switch
}

build_system() {
  parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

  sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos
  sudo ln -s "$parent_path/system/framework/configuration.nix" /etc/nixos/configuration.nix

  sudo nixos-rebuild switch --upgrade
}

build_system
install_home_manager
