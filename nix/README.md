The install script (`build.sh`) is only for linux machines at the moment.

For Apple machines, [install
nix-darwin](https://github.com/LnL7/nix-darwin#install) and symlink the
`darwin-configuration.nix` file (probably in `worktop`? why else would you have
an OSX machine?) to wherever it needs to go for that version of nix-darwin.

## Home-manager
On darwin machines, home-manager is incorporated as a nix-darwin module; you 
will need to add its (master) channel before the system rebuild will work.

On other systems, ome-manager is installed separately and not currently managed
by any of the nix configurations.
