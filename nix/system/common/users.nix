{ config, pkgs, ... }:

{
  programs.zsh.enable = true;

  users.users.haak = {
    isNormalUser = true;
    extraGroups = [
      "wheel" # Enable ‘sudo’ for the user.
      "networkmanager" # Allow the user to manage networks
    ];
  };

  # run the following command to swap escape and caps-lock keys, like a
  # civilized person:
  services.xserver.xkb.options = "caps:swapescape";

  users.extraUsers.haak = {
    shell = pkgs.zsh;
  };
}
