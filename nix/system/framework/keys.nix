let
  ifExists = path: if builtins.pathExists path then path else null;
  readIfExists = path: if builtins.pathExists path then builtins.readFile path else null;
in
{
  ssh = {
    public = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMd7+5+rLGrsGbg+mXjzQLqwAR2VNNFPCb7Va4FqVwd7 haak@framework";
  };

  syncthing = {
    cert = ifExists "/home/haak/dotfiles/secrets/syncthing/cert.pem";
    key = ifExists "/home/haak/dotfiles/secrets/syncthing/key.pem";
  };

  wifi = {
    # These wifi names must match exactly the network names used for
    # wpa_supplicant configuration!
    N904 = {
      pskRaw = readIfExists "/home/haak/dotfiles/secrets/wifi/n904/pskRaw.txt";
    };

    "ShamblingHalfling 5Ghz-1" = {
      pskRaw = readIfExists "/home/haak/dotfiles/secrets/wifi/ShamblingHalfling 5Ghz-1/pskRaw.txt";
    };

    lbheim = {
      pskRaw = readIfExists "/home/haak/dotfiles/secrets/wifi/lbheim/pskRaw.txt";
    };
  };
}
