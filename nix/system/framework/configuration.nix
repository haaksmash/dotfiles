# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      "${builtins.fetchGit {
        url = "https://github.com/NixOS/nixos-hardware.git";
        ref = "master";
        rev = "3980e7816c99d9e4da7a7b762e5b294055b73b2f";
      }}/framework/13-inch/11th-gen-intel"
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../common/users.nix
    ];

  # enable nix-flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  services.fwupd.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  # do not fill the bootloader space with generations until it pops; only keep
  # a reasonable number around.
  boot.loader.systemd-boot.configurationLimit = 42;
  # WiFi support requires kernel 5.13
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.loader.efi.canTouchEfiVariables = true;
  # Enable deep sleep mode
  boot.kernelParams = [ "mem_sleep_default=deep" ];
  # Be able to track CPU temperature.
  boot.kernelModules = [ "coretemp" ];

  hardware.bluetooth.enable = true;
  networking.hostName = "framework"; # Define your hostname.
  networking.networkmanager.enable = true;
  # Create entries for /etc/wpa_supplicant.conf by running `wpa_passphrase SSID PASSWORD`
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp170s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };


  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    cachix
    colmena
    vim
    wget
    zsh
  ];

  nixpkgs.config.allowUnfree = true;

  # Open ports in the firewall.
  networking.firewall = {
    allowedTCPPorts = [
      631 # CUPs port for printing
      4000 # phoenix development port
    ];
    allowedUDPPorts = [
      631
      4000
    ];
  };
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # enable ADB for connecting to android phones
  programs.adb.enable = true;

  # List services that you want to enable:
  services.dbus.packages = with pkgs; [ dconf ];


  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [ pkgs.hplip ];
  };
  # enable avahi for printer discovery
  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # VPN setup
  services.mullvad-vpn.enable = true;
  networking.wireguard.enable = true;

  services.pipewire = {
    enable = false;
    alsa.enable = true;
    jack.enable = true;
    pulse.enable = true;
  };
  services.gnome.gnome-remote-desktop.enable = false;

  # Enable touchpad support.
  services.libinput.enable = true;

  services.xserver = {
    # Enable the X11 windowing system.
    enable = true;

    # Enable the GNOME Desktop Environment.
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;

    xkb = { layout = "us"; };
  };

  services.syncthing = {
    enable = true;
    user = "haak";
    dataDir = "/home/haak/synced";
    configDir = "/home/haak/synced/.config/syncthing";
    cert = (import ./keys.nix).syncthing.cert;
    key = (import ./keys.nix).syncthing.key;
  };

  location.provider = "geoclue2";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.haak.extraGroups = [
    "wheel" # Enable ‘sudo’ for the user.
    "networkmanager" # Allow the user to manage networks
    "adbusers" # allow ADB use
  ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.05"; # Did you read the comment?

  # On 2021-12-01 or thereabouts, a new generation was created that failed to
  # boot; that's a little worrying!
  system.autoUpgrade.enable = false;
}


