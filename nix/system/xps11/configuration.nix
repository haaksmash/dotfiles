# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./glibc-locale-paths.nix
      ../common/users.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 42;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware.bluetooth.enable = true;
  # networking.networkmanager.enable = true;
  networking.hostName = "athena"; # Define your hostname.
  # Create entries for /etc/wpa_supplicant.conf by running `wpa_passphrase SSID PASSWORD`
  networking.wireless.enable = true; # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp58s0.useDHCP = true;
  networking.interfaces.eth0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };
  # run the following command to swap escape and caps-lock keys, like a
  # civilized person:
  # setxkbmap -option caps:swapescape

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    cachix
    p7zip
    wget
  ];

  nixpkgs.config.allowUnfree = true;

  # Open ports in the firewall.
  networking.firewall = {
    allowedTCPPorts = [
      22 # for ssh access
      80
      8080 # port for qbittorrent webui
    ];
  };
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.udev.packages = [
    (pkgs.writeTextFile {
      name = "dev-device-no-auto-name";
      # xps11 uses a usb device as an ethernet port
      text = ''
        SUBSYSTEM=="net", ACTION=="add", DEVTYPE!=="?*", ATTR{address}=="00:e0:4c:68:13:bf", NAME="eth0"
      '';

      destination = "/etc/udev/rules.d/70-persistent-net.rules";
    })
  ];

  services.xserver.layout = "us";

  # xps11 is primarly used as a "server"
  services.logind.lidSwitch = "ignore";

  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
      intel-compute-runtime # OpenCL filter support (hardware tonemapping and subtitle burn-in)
    ];
  };

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?
}
