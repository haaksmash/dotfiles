{ config, pkgs, ... }:
let
  obs-studio = pkgs.callPackage ./makeApp.nix rec {
    name = "OBS";
    version = "29.1.1";
    src = pkgs.fetchurl {
      url = "https://cdn-fastly.obsproject.com/downloads/obs-studio-${version}-macos-arm64.dmg";
      sha256 = "07rhswgx3zqpn32n4ipf34zjjm2cx8fmrb7gliiw6hskwcbbs3yi";
    };
  };

  vlc =
    pkgs.callPackage ./makeApp.nix rec {
      name = "VLC";
      version = "3.0.18";
      src = pkgs.fetchurl {
        url = "https://get.videolan.org/vlc/3.0.18/macosx/vlc-${version}-arm64.dmg";
        sha256 = "0iwgcrwcfyw0r41kjx4hx1sy37mzx5q8nlbzh58gs8ajpjymkhlr";
      };
    };

  keycastr =
    pkgs.callPackage ./makeApp.nix rec {
      name = "KeyCastr";
      version = "0.9.13";
      src = pkgs.fetchurl {
        url = "https://github.com/keycastr/keycastr/releases/download/v${version}/KeyCastr.app.zip";
        sha256 = "1pp0gslq53azhbbyxp12gjy9iaysrf56l16swqdaf4z21p7iag7k";
      };
    };

  discord =
    pkgs.callPackage ./makeApp.nix rec {
      name = "Discord";
      version = "0.0.273";
      src = pkgs.fetchurl {
        url = "https://dl.discordapp.net/apps/osx/${version}/Discord.dmg";
        sha256 = "54794fbf4b29c9a56f6e8a736ff5445c75a1fd3cf49dce7b4d7aa6ff067ae2ef";
      };
    };

  postbird =
    pkgs.callPackage ./makeApp.nix rec {
      name = "Postbird";
      version = "0.8.4";
      src = pkgs.fetchurl {
        url = "https://github.com/Paxa/postbird/releases/download/${version}/Postbird-${version}.dmg";
        sha256 = "1gk7gk3c38rwagx535nl9p0r8drpxfijdiv20qdv7yj81h79db6r";
      };
    };

  whatsapp = pkgs.callPackage ./makeApp.nix rec {
    name = "WhatsApp";
    version = "2.24.6.79";
    unpackCmd = ''
      unzip $curSrc;
    '';
    src = pkgs.fetchurl {
      url = "https://web.whatsapp.com/desktop/mac_native/release/?version=${version}&extension=zip&configuration=Release&branch=relbranch";
      sha256 = "a1efb65baa1730dde4f75ea77bc555dd7e1d9f7573c9b67e88179cca67aaa87a";
    };
  };
in
{
  imports = [ <home-manager/nix-darwin> ];

  nixpkgs.config.allowUnfree = true;
  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages =
    [
      pkgs.vim
      pkgs.obsidian
      pkgs.syncthing
      pkgs.slack
      pkgs._1password
      # pkgs._1password-gui
      pkgs.tailscale
      whatsapp
      obs-studio
      vlc
      keycastr
      discord
      postbird
      pkgs.ngrok

      # wishlist
      # todo (unsupported on aarch64-darwin):
      # pkgs.firefox
      # pkgs.google-chrome
    ];

  services.tailscale.enable = true;

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # have to do this to play nicely with home-manager:
  #   https://github.com/nix-community/home-manager/issues/4026
  users.users.haak.home = "/Users/haak/";
  home-manager.users.haak.imports = [
    ../../home/commandline.nix
    ../../home/programs/alacritty
  ];

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;

  # Create /etc/bashrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  # programs.fish.enable = true;

  # We need this snippet to allow spotlight to find the applications installed
  # by nix-darwin
  system.activationScripts.applications.text = pkgs.lib.mkForce (
    ''
      userDir="$HOME/Applications/Nix Apps"
      echo "setting up $userDir"
      rm -rf $userDir
      mkdir -p $userDir
      chown haak $userDir
      for app in $(find ${config.system.build.applications}/Applications -maxdepth 1 -type l); do
        src="$(/usr/bin/stat -f%Y "$app")"
        dst="$userDir/$(basename $src)"
        echo "app: $app, src: $src, dst: $dst"
        if [[ ! -e $dst ]]; then
          echo "Linking $app"
          ln -s "$app" "$dst"
        fi
      done
    ''
  );

  services.yabai = {
    enable = true;
    package = pkgs.yabai;
    config = {
      # layout
      layout = "bsp";
      auto_balance = "off";
      split_ratio = "0.50";
      window_placement = "second_child";
      # Gaps
      window_gap = 5;
      top_padding = 5;
      bottom_padding = 5;
      left_padding = 5;
      right_padding = 5;
      # appearance, shadows, and borders
      window_shadow = "float";
      active_window_opacity = "1.0";
      normal_window_opacity = "0.90";
      active_window_border_color = "0xE0808080";
      normal_window_border_color = "0x00010101";
      window_border = "on";
      window_border_width = "2";
      # mouse
      mouse_follows_focus = "off";
      focus_follows_mouse = "off";
      mouse_modifier = "fn";
      mouse_action1 = "move";
      mouse_action2 = "resize";
      mouse_drop_action = "swap";
    };

    extraConfig = ''
      # Unload the macOS WindowManager process
      launchctl unload -F /System/Library/LaunchAgents/com.apple.WindowManager.plist > /dev/null 2>&1 &

      # Don't bother tiling certain low-info app windows
      yabai -m rule --add label="Finder" app="^Finder$" manage=off
      yabai -m rule --add label="System Preferences" app="^System Preferences$" title=".*" manage=off
      yabai -m rule --add label="System Settings" app="^System Settings$" title=".*" manage=off
      yabai -m rule --add label="App Store" app="^App Store$" manage=off
      yabai -m rule --add label="Activity Monitor" app="^Activity Monitor$" manage=off
      yabai -m rule --add label="Software Update" title="Software Update" manage=off
      yabai -m rule --add label="About This Mac" app="System Information" title="About This Mac" manage=off
      yabai -m rule --add label="Slack" app="^Slack$" title=".*" manage=off
      yabai -m rule --add label="VLC" app="^VLC media player$" title=".*" manage=off
      yabai -m rule --add label="Discord" app="^Discord$" title=".*" manage=off
      yabai -m rule --add label="1Password" app="^1Password.*$" title=".*" manage=off
      yabai -m rule --add label="Cypress" app="^Cypress.*$" title=".*" manage=off
    '';
  };

  services.skhd = {
    enable = true;
    package = pkgs.skhd;
    skhdConfig = ''
      # Float / Unfloat window
      lalt - space : yabai -m window --toggle float

      # Make window zoom to fullscreen
      shift + lalt - f : yabai -m window --toggle zoom-fullscreen

      # Window Navigation (through display borders)
      lalt - h : yabai -m window --focus west  || yabai -m display --focus west
      lalt - j : yabai -m window --focus south || yabai -m display --focus south
      lalt - k : yabai -m window --focus north || yabai -m display --focus north
      lalt - l : yabai -m window --focus east  || yabai -m display --focus east

      # Window movement
      ## Focused window to left 2/3
      shift + lalt - h : yabai -m window --grid 1:3:0:0:2:1

      # Moving windows between spaces: (Assumes 3 Spaces Max per Display)
      shift + lalt - 1 : DISPLAY="$(yabai -m query --displays --display | jq '.index')";\
                        yabai -m window --space $((1+4*($DISPLAY - 1)))
      shift + lalt - 2 : DISPLAY="$(yabai -m query --displays --display | jq '.index')";\
                        yabai -m window --space $((2+4*($DISPLAY - 1)))
      shift + lalt - 3 : DISPLAY="$(yabai -m query --displays --display | jq '.index')";\
                        yabai -m window --space $((3+4*($DISPLAY - 1)))

      ## Stacks
      # Add the active window to the window or stack to the {direction}
      shift + ctrl - h    : yabai -m window  west --stack $(yabai -m query --windows --window | jq -r '.id')
      shift + ctrl - j    : yabai -m window south --stack $(yabai -m query --windows --window | jq -r '.id')
      shift + ctrl - k    : yabai -m window north --stack $(yabai -m query --windows --window | jq -r '.id')
      shift + ctrl - l    : yabai -m window  east --stack $(yabai -m query --windows --window | jq -r '.id')

      # Stack Navigation: shift + ctrl - {n, p}
      shift + ctrl - n : yabai -m window --focus stack.next
      shift + ctrl - p : yabai -m window --focus stack.prev

      ## Misc
      # Equalize size of windows
      ctrl + lalt - e : yabai -m space --balance
    '';
  };

  launchd.user.agents.syncthing = {
    path = [ pkgs.syncthing ];
    command = "syncthing serve";
    serviceConfig = {
      ProcessType = "Background";
      KeepAlive = true;
    };
  };

  system.keyboard = {
    enableKeyMapping = true;
    remapCapsLockToEscape = true;
  };

  system.defaults = {
    finder.AppleShowAllExtensions = true;

    dock = {
      autohide = true;
      mru-spaces = false;
      showhidden = true;
    };

    # Two-finger scrolling should not cause browsers to navigate!!
    NSGlobalDomain.AppleEnableSwipeNavigateWithScrolls = false;
  };

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
