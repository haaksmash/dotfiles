{ name
, appname ? name
, version
, src
, sourceRoot ? "."
, # from nix
  pkgs
, stdenv
, unzip
, unpackCmd ? ''
      echo "File to unpack: $curSrc";
    if ! [[ "$curSrc" =~ \.dmg$ ]]; then
      echo "Not a .dmg, cannot unpack"
      return 1;
    fi

      mnt=$(mktemp -d -t ci-XXXXXXXXXX)

      function finish {
        echo "Detaching $mnt"
        /usr/bin/hdiutil detach $mnt -force
        rm -rf $mnt
      }
      trap finish EXIT

      echo "Attaching $mnt"
      /usr/bin/hdiutil attach -nobrowse -readonly $src -mountpoint $mnt

      echo "What's in the mount dir"?
      ls -la $mnt/

      echo "Copying contents"
      shopt -s extglob
      DEST="$PWD"
      (cd "$mnt"; cp -a !(Applications) "$DEST/")
  ''
, ...
}:
if stdenv.isDarwin then
  pkgs.stdenv.mkDerivation
  {
    name = "${name}-${version}";
    version = "${version}";
    src = src;

    sourceRoot = "${appname}.app";
    phases = [ "unpackPhase" "installPhase" ];
    unpackCmd = unpackCmd;
    nativeBuildInputs = [ pkgs.unzip ];
    installPhase = ''
      mkdir -p $out/Applications/${appname}.app
      cp -a ./. $out/Applications/${appname}.app/
    '';
  }
else
  { }
