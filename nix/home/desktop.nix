{ pkgs, lib, ... }:

let
  defaultPkgs = with pkgs; [
    calibre
    dmenu
    firefox
    multilockscreen
    mullvad-vpn
    obsidian
    slack
    vlc
    zathura
  ];

  gnomePkgs = with pkgs; [
    gnomecast
    gnome-tweaks
    gnomeExtensions.worksets
  ];
in
{
  imports = [
    (import ./commandline.nix)
    (import ./programs/alacritty)
    # (import ./programs/kitty)
    # (import ./programs/wezterm)
  ];

  nixpkgs.overlays = [ (import ../overlays/paperwm.nix) ];

  home = {
    packages = defaultPkgs ++ gnomePkgs;
  };
}
