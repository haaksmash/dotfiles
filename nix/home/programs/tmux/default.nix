{ ... }:
{
  programs.tmux = {
    enable = true;
    tmuxinator = {
      enable = true;
    };

    terminal = "screen-256color";
    aggressiveResize = true;
    secureSocket = false;
    baseIndex = 1;
    historyLimit = 10000;
    escapeTime = 500;
    mouse = true;

    extraConfig = builtins.readFile ./tmux.conf;
  };
}
