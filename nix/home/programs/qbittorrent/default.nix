{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ qbittorrent-nox ];

  users.groups.qbittorrent = { };
  users.users.qbittorrent.group = "qbittorrent";
  users.extraUsers.qbittorrent = {
    isNormalUser = true;
    extraGroups = [ "users" "multimedia" ];
  };

  systemd = {
    packages = [ pkgs.qbittorrent-nox ];

    services."qbittorrent-nox@fc" = {
      enable = true;
      serviceConfig = {
        Type = "simple";
        User = "qbittorrent";
        ExecStart = "${pkgs.qbittorrent-nox}/bin/qbittorrent-nox";
      };
      wantedBy = [ "multi-user.target" ];
    };
  };

  networking.firewall.allowedTCPPorts = [ 8080 ];

}
