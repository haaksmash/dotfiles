{lib, ...}:

let
 domain = lib.strings.fileContents ../../../../secrets/letsencrypt/mediaserver/forgedomain;
 port = 9090;
in
{
  networking.firewall = {
    allowedTCPPorts = [ port ];
    allowedUDPPorts = [ port ];
  };

  services.forgejo = {
    enable = true;
    # database.type = "postgres";
    # Enable support for Git Large File Storage
    lfs.enable = true;
    settings = {
      server = {
        DOMAIN = domain;
        # You need to specify this to remove the port from URLs in the web UI.
        ROOT_URL = "https://${domain}/";
        HTTP_PORT = port;
      };
      # You can temporarily allow registration to create an admin user.
      service.DISABLE_REGISTRATION = true;
    };
  };
}
