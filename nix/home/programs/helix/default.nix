{ pkgs, ... }:
{
  programs.helix = {
    enable = true;
    languages = {
      language-server = {
        nixd = {
          command = "${pkgs.nixd}/bin/rnix-lsp";
        };

        typescript-language-server = {
          command = "typescript-language-server";
          args = [ "--stdio" ];
          config.hostInfo = "helix";
        };
      };

      language = [
        {
          name = "nix";
          auto-format = true;
          language-servers = [
            { name = "nixd"; }
          ];
        }
        {
          name = "typescript";
          auto-format = true;
          language-servers = [
            {
              name = "typescript-language-server";
              except-features = [ "format" "diagnostics" ];
            }
          ];
        }
      ];
    };
    settings = {
      theme = "catppuccin_mocha";
      editor = {
        line-number = "relative";
        lsp.display-messages = true;
        cursor-shape = {
          insert = "underline";
          normal = "block";
          select = "block";
        };
      };
      keys = {
        normal = {
          g = { a = "code_action"; }; # Maps `ga` to show possible code actions
          "ret" = [ "move_line_down" "goto_first_nonwhitespace" ]; # Maps the enter key to move to start of next line
          X = "extend_line_above";
          D = "delete_char_backward";
          ";" = "command_mode"; # Note: this shadows the normal use of ";", which is to reset the selection to the cursor.
          "S-ret" = [ "move_line_up" "goto_first_nonwhitespace" ];
        };
        insert = {
          j = { j = "normal_mode"; }; # Maps `jj` to exit insert mode
        };
      };

    };
  };
}
