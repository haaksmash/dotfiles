{ pkgs, ... }:
{
  users.groups.multimedia = { };
  services.calibre-server = {
    enable = true;
    port = 8780;
    libraries = [
      "/storage/organized/books"
    ];
    auth = {
      enable = true;
      userDb = "/storage/organized/books/users.sqlite";
    };
  };

  networking.firewall.allowedTCPPorts = [ 8780 ];

  users.extraUsers."calibre-server" = {
    extraGroups = [ "calibre-server" "multimedia" ];
  };
}
