{ pkgs, ... }:
let
  vim-nix = pkgs.vimUtils.buildVimPlugin {
    name = "vim-nix";
    src = pkgs.fetchFromGitHub {
      owner = "LnL7";
      repo = "vim-nix";
      rev = "d733cb96707a2a6bdc6102b6d89f947688e0e959";
      sha256 = "12k2gwm8b1n02361gpjhsldql99yd7qsh80a3lbwc5c5gz5j8jsb";
    };
  };

  vim-precognition = pkgs.vimUtils.buildVimPlugin {
    name = "vim-precognition";
    version = "2024-05-26";
    src = pkgs.fetchFromGitHub {
      owner = "tris203";
      repo = "precognition.nvim";
      rev = "b40c3539f95504bea2ac4ac4dc866a95edba6d4d";
      sha256 = "sha256-fyaMWL0MsmWJ8KcY1QkbtoZ7tDbIbFbEYgA/ZsOoVSk=";
    };
  };
in
with builtins;
{
  programs.neovim = {
    enable = true;
    vimAlias = true;
    plugins = with pkgs.vimPlugins; [
      vim-sensible
      vim-startify
      catppuccin-nvim
      vim-precognition
      nvim-treesitter.withAllGrammars

      {
        plugin = fzf-vim;
        config = ''
          noremap <leader>fc :BCommits<CR>
        '';
      }
      vim-elixir
      alchemist-vim
      vim-nix
      vim-javascript
      vim-json
      vim-jsx-pretty

      vim-commentary
      vim-airline
      {
        plugin = rainbow;
        config = ''
          let g:rainbow_conf = {
          \ 'guifgs': ['RoyalBlue3', 'SeaGreen3', 'DarkOrchid3', 'firebrick3', 'RoyalBlue3', 'SeaGreen3', 'DarkOrchid3', 'firebrick3', 'RoyalBlue3', 'DarkOrchid3', 'firebrick3', 'RoyalBlue3', 'SeaGreen3', 'DarkOrchid3', 'firebrick3'],
          \ 'ctermfgs': ['red', 'brown', 'blue', 'gray', 'green', 'magenta', 'cyan', 'darkred', 'brown', 'darkblue', 'gray', 'darkgreen', 'darkmagenta', 'darkcyan', 'red'],
          \ 'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold']
          \}
          let g:rainbow_active = 1
        '';
      }
      {
        plugin = vim-indent-guides;
        config = ''
          let g:indent_guides_enable_on_vim_startup = 1
          let g:indent_guides_start_level = 2
        '';
      }
      vim-surround
      {
        plugin = vim-fugitive;
        config = ''
          nnoremap gb :Git blame<CR>
        '';
      }
      {
        plugin = git-blame-nvim;
        config = ''
          let g:gitblame_date_format = '%Y-%m-%d'
          nnoremap gC :GitBlameOpenCommitURL<CR>
        '';
      }
      {
        plugin = neogit;
        config = ''
          noremap <leader>g :Neogit<CR>
          lua << EOF
            local neogit = require('neogit')
            neogit.setup {}
          EOF
        '';
      }
      vinegar
      {
        plugin = gitsigns-nvim;
        config = ''
          lua << EOF
            require('gitsigns').setup()
          EOF
        '';
      }
      {
        plugin = undotree;
        config = ''
          nnoremap <leader>u :UndotreeToggle<CR>
          let g:undotree_SetFocusWhenToggle = 1
          if has("persistent_undo")
             let target_path = expand('~/.undodir')

              " create the directory and any parent directories
              " if the location does not exist.
              if !isdirectory(target_path)
                  call mkdir(target_path, "p", 0700)
              endif

              let &undodir=target_path
              set undofile
          endif
        '';
      }
      cmp-nvim-lsp
      {
        plugin = nvim-lspconfig;
        config = ''
            lua << EOF
            ${readFile (
            pkgs.substituteAll {
              src = ./nvim-lspconfig.lua;
              # since both of these are used for actually configuring the system,
              # we'll probably always want them around.
              lua_ls = pkgs.sumneko-lua-language-server;
              nixd = pkgs.nixd;
            }
          ) }
            EOF
        '';
      }
      {
        plugin = telescope-nvim;
        config = ''
          nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
          nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
          nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
          nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>
        '';
      }
      # have the completion plugin get loaded last just in case anything above
      # needs to do setup work before completions get set up.
      {
        plugin = nvim-cmp;
        config = ''
          lua << EOF
          ${ readFile ./nvim-cmp.lua }
          EOF
        '';
      }
    ];

    extraConfig = readFile ./init.vim;
  };

}
