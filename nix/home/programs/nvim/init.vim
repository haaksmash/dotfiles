filetype plugin indent on

" Indent guides

" let netrw look like NERDTree
let g:netrw_liststyle = 3
set nobackup
set noswapfile
set pastetoggle=<F3>
set nowrap

set background=dark
colorscheme catppuccin

set nocompatible
set number relativenumber
set numberwidth=1
set cursorline
set cursorcolumn
autocmd WinEnter * if !exists('w:overLengthMatchId') | let w:overLengthMatchId = matchadd('OverLength', '\%81v.\+\%<122v') | endif
autocmd WinEnter * if !exists('w:reallyOverLengthMatchId') | let w:reallyOverLengthMatchId = matchadd('ReallyOverLength', '\%121v.\+') | endif
highlight OverLength ctermbg=DarkRed ctermfg=white guibg=#352e10
highlight ReallyOverLength ctermbg=Red ctermfg=white guibg=#330000
set termguicolors
set ruler
set list
set listchars=tab:>-,trail:-,extends:>,precedes:<,nbsp:+,eol:$
set matchpairs+=<:>
set ts=2
set shiftwidth=2
set expandtab

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

""" Searching and Patterns
set ignorecase
set smartcase
set smarttab
set hlsearch
" Add the g flag to search/replace by default
set gdefault

"recalculate the trailing whitespace warning when idle, and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

" strip trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e

""" Handy remaps
noremap ; :
inoremap jj <Esc>
noremap Q <nop>
noremap ? :lua require("precognition").peek()<CR>

""" Esperanto digraphs (for use with ctrl-k)
:digraph Cx 264
:digraph cx 265
:digraph Gx 284
:digraph gx 285
:digraph Hx 292
:digraph hx 293
:digraph Jx 308
:digraph jx 309
:digraph Sx 348
:digraph sx 349
:digraph Ux 364
:digraph ux 365

""" Terminal mode remaps
tnoremap <Esc> <C-\><C-n>

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Don’t reset cursor to start of line when moving around.
set nostartofline
" minimal number of lines to keep above/below cursorline
set scrolloff=10

" Local overrides?
if filereadable($HOME . "/.vimrc.local")
  source ~/.vimrc.local
endif

lua  << EOF
require'nvim-treesitter.configs'.setup {
   highlight = {
      enable = true,
   },
 }
EOF
