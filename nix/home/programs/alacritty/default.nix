{ pkgs, ... }:
let
in
with builtins;
{
  programs.alacritty = {
    enable = true;
    settings = {
      window.decorations = "none";
      font.size = 14;
      selection.save_to_clipboard = true;
    };
  };
}
