{ pkgs, ... }:

with builtins; {

  home.packages = with pkgs; [
    zsh-autosuggestions
    zsh-syntax-highlighting
  ];

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    defaultKeymap = "viins";

    initExtra = readFile ./zshrc;

    oh-my-zsh = {
      enable = true;
      theme = "typewritten";
      custom = "$HOME/.oh-my-zsh/custom";
      plugins = [
        "vi-mode"
      ];
    };
  };

  home.file.".oh-my-zsh/custom/themes/typewritten.zsh-theme" = {
    source = ./typewritten.zsh-theme;
  };
}
