{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ beets-unstable ];

  # If using home-manager, we could the beets settings in a more in-nix way. As
  # it is, here's the YAML that's currently on the NAS. If being serious, we
  # would want to fill this in with interpolated values to be machine-agnostic
  #
  # plugins: spotify convert missing
  # directory: /storage/Music
  #
  # convert:
  #   auto: no
  #   dest: /storage/Music
  #
  # import:
  #   resume: yes
  #   copy: yes
  #   write: yes
  #   incremental: yes
  #   incremental_skip_later: no
}
