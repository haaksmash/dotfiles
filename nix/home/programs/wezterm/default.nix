{ pkgs, ... }:
{
  programs.wezterm = {
    enable = true;
    enableZshIntegration = true;
    extraConfig = ''
      -- Pull in the wezterm API
      local wezterm = require 'wezterm';

      -- This will hold the configuration.
      local config = wezterm.config_builder();

      config.unix_domains = {
        {
          name = 'unix',
        },
      };

      -- This causes `wezterm` to act as though it was started as
      -- `wezterm connect unix` by default, connecting to the unix
      -- domain on startup.
      -- If you prefer to connect manually, leave out this line.
      config.default_gui_startup_args = { 'connect', 'unix' };

      config.font_size = 14.0;
      config.hide_tab_bar_if_only_one_tab = true;
      config.tab_bar_at_bottom=true;

      return config;
    '';
  };
}
