{lib, ...}:
let
  acmeEmail = lib.strings.fileContents ../../../../secrets/letsencrypt/mediaserver/email;
in
{
  security.acme.acceptTerms = true;
  security.acme.defaults.email = acmeEmail;
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
  };
}
