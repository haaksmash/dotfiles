{ pkgs, ... }:
let
in
with builtins;
{
  programs.direnv = {
    enable = true;
    stdlib = readFile ./direnvrc;
  };
}
