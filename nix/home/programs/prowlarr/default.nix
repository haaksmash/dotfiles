{ pgks, ... }:
{
  services.prowlarr = {
    enable = true;
    openFirewall = true;
  };
}
