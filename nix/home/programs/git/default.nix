{ pkgs, ... }:

let
in
{
  programs.git = {
    enable = true;
    userName = "Haak Saxberg";
    userEmail = "haak.erling@gmail.com";
    delta = {
      enable = true;
      options = {
        side-by-side = true;
        line-numbers = true;
      };
    };
    extraConfig = {
      rebase = {
        updateRefs = true;
      };

      init = {
        defaultBranch = "main";
      };
    };

    ignores = [
      ".idea/"
      ".direnv/"
      ".git-plans/"
      ".envrc"
      ".tools-info"
      "*~"
      "*.swp"
      ".yardoc/"
      "doc/"
      ".DS_Store"
      ".jj/"
    ];
  };

  programs.nushell.shellAliases = {
    gst = "git status";
    gco = "git checkout";
    gf = "git fetch";
    gl = "git pull";
  };
  programs.nushell.extraConfig = ''
    def glog [] { ^git log --pretty=%h»¦«%s»¦«%aN»¦«%aE»¦«%aD -n 25 | lines | split column "»¦«" commit subject name email date | upsert date {|d| $d.date | into datetime} | sort-by date | reverse | explore }
  '';

  programs.zsh.shellAliases = {
    gst = "git status";
    glog = "git log --oneline --graph --decorate";
    gco = "git checkout";
    gf = "git fetch";
    gl = "git pull";
  };
}
