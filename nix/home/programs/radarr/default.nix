{ pkgs, ... }:
{
  users.groups.multimedia = { };

  services.radarr = {
    enable = true;
    openFirewall = true;
  };

  users.extraUsers.radarr = {
    extraGroups = [ "radarr" "multimedia" ];
  };

}
