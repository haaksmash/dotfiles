{ pkgs, ... }:
{
  users.groups.multimedia = { };

  services.readarr = {
    enable = true;
    openFirewall = true;
  };

  users.extraUsers.readarr = {
    extraGroups = [ "readarr" "multimedia" ];
  };
}
