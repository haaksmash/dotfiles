{ pkgs, ... }:
{
  users.groups.multimedia = { };

  services.bazarr = {
    enable = true;
    openFirewall = true;
  };

  users.extraUsers.bazarr = {
    extraGroups = [ "bazarr" "multimedia" ];
  };
}
