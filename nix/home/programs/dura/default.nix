{ pkgs, ... }:

with builtins; {
  home.packages = with pkgs; [
    dura
  ];
}
