{ pkgs, ... }:

let
  defaultPkgs = with pkgs; [
    p7zip
  ];
in
{
  imports = [
    (import ./programs/dura)
    (import ./universal.nix)
  ];

  home.packages = defaultPkgs;

  xdg.enable = true;

  # Auto-completion helper
  programs.carapace = {
    enable = true;
    enableNushellIntegration = true;
  };

  programs.starship = {
    enable = true;
    settings = {
      add_newline = true;
      character = {
        success_symbol = "[➜](bold green)";
        error_symbol = "[➜](bold red)";
      };

      directory = {
        truncation_length = 100;
        truncate_to_repo = false;
      };

      battery = {
        disabled = true;
        full_symbol = "• ";
        charging_symbol = "⇡ ";
        discharging_symbol = "⇣ ";
        unknown_symbol = "❓ ";
        empty_symbol = "❗ ";
      };

      erlang = {
        symbol = "ⓔ ";
      };

      nodejs = {
        symbol = "[⬢](bold green) ";
      };

      pulumi = {
        symbol = "🧊 ";
      };
    };
  };
}
