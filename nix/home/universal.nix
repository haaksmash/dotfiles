{ pkgs, ... }:

let
  git-plan = pkgs.rustPlatform.buildRustPackage rec {
    name = "git-plan";
    version = "1";
    src = pkgs.fetchFromGitLab {
      owner = "haaksmash";
      repo = "git-plan";
      rev = "518dcf15f3a46605f18569dd69b3b63d763e9439";
      sha256 = "06gh3v35bdr0qsp459183br4hpmwm36d2r9fjyx9j5jfpzggqg51";
    };

    cargoSha256 = "17lr4pc2fjcgvx0p4vpkwx3mg9jadb4lygbwri0blmnkqql8xlh3";
    nativeBuildInputs = [ pkgs.pkgconfig ];
    buildInputs = [ pkgs.openssl ] ++ (if pkgs.stdenv.isDarwin then [ pkgs.libiconv pkgs.darwin.apple_sdk.frameworks.Security ] else [ ]);
  };

  defaultPkgs = with pkgs; [
    # git-plan
    jq
    tree
    ripgrep
  ];
in
with builtins;
{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  home.username = "haak";
  home.homeDirectory = if pkgs.stdenv.isDarwin then "/Users/haak/" else "/home/haak";
  home.stateVersion = "22.05";

  # no need for home-manager news, thanks
  news.display = "silent";

  nixpkgs.config = {
    allowUnfree = true;
  };

  imports = [
    (import ./programs/direnv)
    (import ./programs/jj)
    (import ./programs/git)
    (import ./programs/zsh)
    (import ./programs/tmux)
    (import ./programs/nvim)
    (import ./programs/helix)
    (import ./programs/yazi)
  ];

  home.packages = defaultPkgs;

  programs.htop = {
    enable = true;
  };

  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
    defaultCommand = "rg --no-ignore --follow --files --hidden --smart-case --glob \\\"!.git/*\\\"";
  };

}
